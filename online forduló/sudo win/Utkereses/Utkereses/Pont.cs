﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utkereses
{
    class Pont
    {
        int x;

        public int X
        {
            get { return x; }
            set { x = value; }
        }
        int y;

        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        int sorSzam;

        public int SorSzam
        {
            get { return sorSzam; }
            set { sorSzam = value; }
        }

        public Pont(int x, int y)
        {
            this.x = x;
            this.y = y;
            sorSzam = 0;
        }

        public Pont(int x, int y, int sorSzam)
        {
            this.x = x;
            this.y = y;
            this.sorSzam = sorSzam;
        }
    }
}
