﻿using System;
using System.IO;
using System.Text;
using System.Drawing;
using System.Collections.Generic;

namespace KepAnalizis_3
{
    class Program
    {
        const string fajlnev = "minta.bmp";
        static Bitmap bitmap = (Bitmap)Image.FromFile(fajlnev, true);
        static Color[,] kep = _1Beolvas();
        static bool[,] feldolgozva = new bool[kep.GetLength(0), kep.GetLength(1)];
        static List<Terulet> teruletek = new List<Terulet>();


        static void Main()
        {
            System.Diagnostics.Process.Start(fajlnev);

            Console.WriteLine("Első feladat: ");
            ElsoFeladat();
            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Második feladat: ");
            MasodikFeladat();
            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Harmadik feladat: ");
            HarmadikFeladat();
            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("Negyedik feladat: ");
            NegyedikFeladat();
            Console.ReadLine();
            Console.Clear();

            Console.WriteLine("\n\nvége");
            Console.ReadLine();
        }
        static Color[,] _1Beolvas()
        {
            Color[,] tomb = new Color[bitmap.Height, bitmap.Width];
            for (int a = 0; a < bitmap.Height; a++)
            {
                for (int b = 0; b < bitmap.Width; b++)
                {
                    Color pixel = bitmap.GetPixel(b, a); //x koordináta, y koordináta. Azért kell felcserélve
                    tomb[a, b] = pixel;
                }
            }

            return tomb;
        }

//1. feladat:
        static void ElsoFeladat()
        {
            //1. feladat:
            //Mekkora és hol van a legnagyobb összefüggő terület a képen? (legnagyobb alatt a legtöbb pixelből állót értjük)

            //Az algoritmus több legnagyobb terület esetén csak egyet ad vissza.

            #region Mekkora
            for (int kulsoX = 0; kulsoX < kep.GetLength(0); kulsoX++) //végigmegy minden soron
            {
                for (int kulsoY = 0; kulsoY < kep.GetLength(1); kulsoY++) //azon belül minden elemen (oszlopon)
                {
                    if (!feldolgozva[kulsoX, kulsoY]) //ha még nem volt feldolgozva az adott pixel
                    {
                        Terulet t = new Terulet();
                        teruletek.Add(t);

                        Queue<Pont> pixelekSora = new Queue<Pont>();
                        pixelekSora.Enqueue(new Pont(kulsoX, kulsoY));

                        while (pixelekSora.Count > 0)
                        {
                            Pont p = pixelekSora.Dequeue();
                            int x = p.X, y = p.Y;

                            t.Pixelek.Add(kep[x, y]); //a hivatkozott pixel hozzáadása a jelenlegi területhez
                            t.Pixelhelyek.Add(new Pont(x, y));
                            feldolgozva[x, y] = true; //itt kell az aktuális pixelt feldolgozottra állítani

                            if (BejarhatoNemFeldolgozott(x, y + 1) && ToleUgyanolyanSzinu(x, y, Merre.jobbra))
                            {
                                Color akt = kep[x, y + 1];
                                if (!pixelekSora.Contains(new Pont(x, y + 1)))
                                    pixelekSora.Enqueue(new Pont(x, y + 1));
                            }
                            if (BejarhatoNemFeldolgozott(x + 1, y) && ToleUgyanolyanSzinu(x, y, Merre.lefele))
                            {
                                Color akt = kep[x + 1, y];
                                if (!pixelekSora.Contains(new Pont(x + 1, y)))
                                    pixelekSora.Enqueue(new Pont(x + 1, y));
                            }
                            if (BejarhatoNemFeldolgozott(x, y - 1) && ToleUgyanolyanSzinu(x, y, Merre.balra))
                            {
                                Color akt = kep[x, y - 1];
                                if (!pixelekSora.Contains(new Pont(x, y - 1)))
                                    pixelekSora.Enqueue(new Pont(x, y - 1));
                            }

                            if (BejarhatoNemFeldolgozott(x - 1, y) && ToleUgyanolyanSzinu(x, y, Merre.felfele))
                            {
                                Color akt = kep[x - 1, y];
                                if (!pixelekSora.Contains(new Pont(x - 1, y)))
                                    pixelekSora.Enqueue(new Pont(x - 1, y));
                            }
                        }
                    }
                }
            }

            //OsszesTeruletMeretKiir(); //Ha kell az összes terület mérete
            int legnagyobbmeret = LegnagyobbTeruletMerete(); //hogy ne kelljen kétszer hívni a függvényt
            Console.WriteLine("A legnagyobb méretű terület {0} pixelnyi.", legnagyobbmeret);
            #endregion Mekkora

            #region Hol van
            Console.WriteLine("\nFoglaljuk a lehető legkisebb téglalapba a legnagyobb területet.");
            Terulet legnagyobbTerulet = TeruletKeresMeretAlapjan(legnagyobbmeret);
            TeruletSarkaiKiir(legnagyobbTerulet);
            #endregion
        }
        /// <summary>
        /// Megadja, hogy a tőle balra/jobbra/felfelé/lefelé lévő pixel azonos színű -e a jelenlegivel.
        /// </summary>
        /// <param name="x">sor</param>
        /// <param name="y">oszlop</param>
        /// <param name="irany">hozzá képest milyen irányban nézzük</param>
        /// <returns></returns>
        static bool ToleUgyanolyanSzinu(int x, int y, Merre irany)
        {
            if (irany == Merre.balra)
                return (kep[x, y].Equals(kep[x, y - 1]));
            else if (irany == Merre.jobbra)
                return (kep[x, y].Equals(kep[x, y + 1]));
            else if (irany == Merre.felfele)
                return (kep[x, y].Equals(kep[x - 1, y]));
            else //lefele
                return (kep[x, y].Equals(kep[x + 1, y]));
        }
        /// <summary>
        /// Megvizsgálja, hogy az adott x,y koordináta vizsgálható -e (azaz létezik -e), és ha igen akkor
        ///  ellenőriztük -e már.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        static bool BejarhatoNemFeldolgozott(int x, int y)
        {
            if (x < 0 || x >= kep.GetLength(0) || y < 0 || y >= kep.GetLength(1) || feldolgozva[x,y])
                return false;
            else
                return true;
        }
        static void OsszesTeruletMeretKiir()
        {
            Console.WriteLine("A területek méreteinek listája: ");
            for (int i = 0; i < teruletek.Count; i++)
                Console.Write(teruletek[i].TeruletMeret + "  ");
            Console.WriteLine();
        }
        static int LegnagyobbTeruletMerete()
        {
            int max = 0;
            for (int i = 0; i < teruletek.Count; i++)
                if (teruletek[i].TeruletMeret > max)
                    max = teruletek[i].TeruletMeret;
            return max;
        }
        static Terulet TeruletKeresMeretAlapjan(int keresendo_meret)
        {
            for (int i = 0; i < teruletek.Count; i++)
            {
                if (teruletek[i].TeruletMeret == keresendo_meret)
                    return teruletek[i];
            }
            return null;
        }
        static void TeruletSarkaiKiir(Terulet t)
        {
            int minX = int.MaxValue, maxX = -1, minY = int.MaxValue, maxY = -1;

            for (int i = 0; i < t.TeruletMeret; i++)
            {
                if (t.Pixelhelyek[i].X < minX)
                    minX = t.Pixelhelyek[i].X;
                else if (t.Pixelhelyek[i].X > maxX)
                    maxX = t.Pixelhelyek[i].X;
                if (t.Pixelhelyek[i].Y < minY)
                    minY = t.Pixelhelyek[i].Y;
                else if (t.Pixelhelyek[i].Y > maxY)
                    maxY = t.Pixelhelyek[i].Y;
            }

            Console.WriteLine("Ezen terület:");
            Console.WriteLine("- bal felső sarka a ({0},{1}) koordinátán\n- jobb alsó sarka a ({2},{3}) koordinátán helyezkedik el.", minX + 1, minY + 1, maxX + 1, maxY + 1);
            Console.WriteLine("(a koordinátákat 1-től indexeljük)");
        }

//2. feladat:
        static void MasodikFeladat()
        {
            //Definiáljuk az átlót a következőképpen:
            //Az átló a két legtávolabbi pont távolsága.
            //függetlenül attól, hogy az átmérő (mint vonal) mentén milyen más területek pontjai vannak.

            double maxAtlo;
            Terulet maxAtloTerulet = new Terulet();
            maxAtlo = 0;
            double teruletMax;
            for (int i = 0; i < teruletek.Count; i++)
            {
                teruletMax = 0;
                Terulet t = teruletek[i];
                for (int j = 0; j < t.Pixelhelyek.Count; j++)
                {
                    Pont p1 = t.Pixelhelyek[j];
                    for (int k = 0; k < t.Pixelhelyek.Count; k++)
                        if (k != j)
                        {
                            Pont p2 = t.Pixelhelyek[k];
                            if (Tavolsag(p1, p2) > teruletMax)
                                teruletMax = Tavolsag(p1, p2);
                        }
                }
                if (teruletMax >= maxAtlo)
                {
                    maxAtlo = teruletMax;
                    maxAtloTerulet = teruletek[i];
                }
            }

            Console.WriteLine("A legnagyobb átmérőjű terület átlójának mérete: {0:f4}", maxAtlo);
            Console.WriteLine("\nFoglaljuk a lehető legkisebb téglalapba a legnagyobb átmérőjű területet.");
            TeruletSarkaiKiir(maxAtloTerulet);
        }
        static double Tavolsag(Pont a, Pont b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }

//3. feladat:
        static void HarmadikFeladat()
        {
            //Jelöljük ki mindegyik területobjektum kerületét, azaz határozzuk meg a belső kontúr kerületét
            for (int i = 0; i < teruletek.Count; i++)
                teruletek[i].Keruletkijeloles();

            int maxkerIndex = 0;
            for (int i = 0; i < teruletek.Count; i++)
                if (teruletek[i].KeruletMeret > teruletek[maxkerIndex].KeruletMeret)
                    maxkerIndex = i;
            Console.WriteLine("A legnagyobb kerülettel rendelkező terület kerületének mérete: {0}", teruletek[maxkerIndex].KeruletMeret);
            Console.WriteLine("\nFoglaljuk a lehető legkisebb téglalapba a legnagyobb kerülettel rendelkező területet.");
            TeruletSarkaiKiir(teruletek[maxkerIndex]);
        }

//4. feladat:
        static void NegyedikFeladat()
        {
            Terulet legnagyobb = new Terulet();
            Terulet legnagyobb2 = new Terulet();
            int legnagyobbTerulet = 0;
            for (int i = 0; i < teruletek.Count; i++)
            {
                bool egyforma = true;
                bool vanazonosmeretu = false;
                Terulet t1 = teruletek[i];
                Pont p1 = NegyzetMerete(t1);
                for (int j = 0; j < teruletek.Count; j++)
                {
                    Terulet t2 = teruletek[j];
                    Pont p2 = NegyzetMerete(t2);
                    if (p1.X == p2.X && p1.Y == p2.Y && j != i)
                    {
                        vanazonosmeretu = true;
                        for (int k = 0; k < t1.Pixelhelyek.Count; k++)
                        {
                            Pont p3 = new Pont(t1.Pixelhelyek[k].X, t1.Pixelhelyek[k].Y);
                            p3.X -= Legkisebbek(t1).X;
                            p3.Y -= Legkisebbek(t1).Y;
                            bool van = false;
                            for (int l = 0; l < t2.Pixelhelyek.Count; l++)
                            {
                                Pont p4 = new Pont(t2.Pixelhelyek[l].X, t2.Pixelhelyek[l].Y);
                                p4.X -= Legkisebbek(t2).X;
                                p4.Y -= Legkisebbek(t2).Y;

                                if (p3.X == p4.X && p3.Y == p4.Y)
                                    van = true;
                            }
                            if (!van)
                                egyforma = false;
                        }
                    }
                    legnagyobb2 = t2;

                }
                if (egyforma && vanazonosmeretu && t1.TeruletMeret > legnagyobbTerulet)
                {
                    legnagyobbTerulet = t1.TeruletMeret;
                    legnagyobb = t1;
                }
            }
            if (legnagyobbTerulet == 0)
            {
                Console.WriteLine("Nincs két egyforma terület");
            }
            else
            {
                Console.WriteLine("A legnagyobb egyforma területek mérete: " + legnagyobb.TeruletMeret);
                Console.WriteLine("\nFoglaljuk a lehető legkisebb téglalapba a két megfelelő területeket, ezek felsorolása:");
                TeruletSarkaiKiir(legnagyobb);
                Console.WriteLine();
                TeruletSarkaiKiir(legnagyobb2);
            }
        }
        static Pont NegyzetMerete(Terulet t)
        {
            int minX = int.MaxValue, maxX = -1, minY = int.MaxValue, maxY = -1;

            for (int i = 0; i < t.TeruletMeret; i++)
            {
                if (t.Pixelhelyek[i].X < minX)
                    minX = t.Pixelhelyek[i].X;
                else if (t.Pixelhelyek[i].X > maxX)
                    maxX = t.Pixelhelyek[i].X;
                if (t.Pixelhelyek[i].Y < minY)
                    minY = t.Pixelhelyek[i].Y;
                else if (t.Pixelhelyek[i].Y > maxY)
                    maxY = t.Pixelhelyek[i].Y;
            }
            return new Pont(maxX - minX, maxY - minY);
        }
        static Pont Legkisebbek(Terulet t)
        {
            int minX = int.MaxValue, maxX = -1, minY = int.MaxValue, maxY = -1;

            for (int i = 0; i < t.TeruletMeret; i++)
            {
                if (t.Pixelhelyek[i].X < minX)
                    minX = t.Pixelhelyek[i].X;
                else if (t.Pixelhelyek[i].X > maxX)
                    maxX = t.Pixelhelyek[i].X;
                if (t.Pixelhelyek[i].Y < minY)
                    minY = t.Pixelhelyek[i].Y;
                else if (t.Pixelhelyek[i].Y > maxY)
                    maxY = t.Pixelhelyek[i].Y;
            }
            return new Pont(minX, minY);
        }
    }
    enum Merre { balra, jobbra, felfele, lefele };
}