﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace KepAnalizis_3
{
    class Terulet
    {
        List<Color> pixelek = new List<Color>();
        List<Pont> pixelhelyek = new List<Pont>();

        List<Pont> keruletpixelhelyek = new List<Pont>();

        public List<Color> Pixelek
        {
            get { return pixelek; }
            set { pixelek = value; }
        }
        public List<Pont> Pixelhelyek
        {
            get { return pixelhelyek; }
            set { pixelhelyek = value; }
        }
        public List<Pont> Keruletpixelhelyek
        {
            get { return keruletpixelhelyek; }
            set { keruletpixelhelyek = value; }
        }

        public Color Szin
        {
            get
            {
                if (pixelek.Count == 0)
                    System.Windows.Forms.MessageBox.Show("Üres területre ne hívd meg a Szin tulajdonságot!");

                return pixelek[0];
            }
        }
        public int TeruletMeret
        {
            get { return pixelek.Count; }
        }
        public int KeruletMeret
        {
            get { return keruletpixelhelyek.Count; }
        }
        public override string ToString()
        {
            return String.Format("Méret: {0}, Szín: {1}.", TeruletMeret, Szin);
        }

        /// <summary>
        /// Átmásol minden olyan pontot a pixelhelyekből, amelyre igaz, hogy az alakzat szélén helyezkedik el.
        /// </summary>
        public void Keruletkijeloles()
        {
            //minden olyan pont kerületi pont, amelyre NEM igaz az, hogy mindegyik oldalán található pont.
            keruletpixelhelyek = pixelhelyek.ToList(); //klónozza a pixelhelyek-et a keruletpixelhelyek-be
            List<Pont> kerulet = new List<Pont>();

            for (int i = 0; i < keruletpixelhelyek.Count; i++)
            {
                Pont p = new Pont(keruletpixelhelyek[i].X, keruletpixelhelyek[i].Y);
                if (!MindegyikOldalanVanEPont(p))
                //ha nem mindegyik oldalán van pont, akkor ő külső pont, tehát kell a kerületbe.
                    kerulet.Add(p);
            }
            keruletpixelhelyek = kerulet;
            //így a keruletpixelhelyekbe kerül a belső kontúrok pixelszáma.
        }
        private bool MindegyikOldalanVanEPont(Pont p) //Csabi írta
        {
            int szomszedok = 0;
            for (int i = 0; i < keruletpixelhelyek.Count; i++)
            {
                Pont p2 = keruletpixelhelyek.ElementAt(i);
                if (Math.Abs(p.X - p2.X) + Math.Abs(p.Y - p2.Y) == 1)
                    szomszedok++;
            }
            return szomszedok == 4;
        }
    }
}