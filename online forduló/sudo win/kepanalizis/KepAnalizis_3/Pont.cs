﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace KepAnalizis_3
{
    class Pont : ICloneable
    {
        public Pont(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        int x;
        public int X
        {
            get { return x; }
            set { x = value; }
        }

        int y;
        public int Y
        {
            get { return y; }
            set { y = value; }
        }

        public override bool Equals(object obj)
        {
            return (this.X == (obj as Pont).X && this.Y == (obj as Pont).Y);
        }
        public override string ToString()
        {
            return string.Format("{0},{1}", x, y);
        }

        public object Clone()
        {
            return new Pont(x, y);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
