﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Comparison
    {
        bool attributes_count;
        public bool Attributes_count
        {
            get { return attributes_count; }
            set { attributes_count = value; }
        }

        bool children_count;
        public bool Children_count
        {
            get { return children_count; }
            set { children_count = value; }
        }

        bool elementName;
        public bool ElementName
        {
            get { return elementName; }
            set { elementName = value; }
        }

        bool text;
        public bool Text
        {
            get { return text; }
            set { text = value; }
        }

        public bool AllTrue
        {
            get
            {
                return attributes_count && children_count && ElementName && Text;
            }
        }
    }
}
