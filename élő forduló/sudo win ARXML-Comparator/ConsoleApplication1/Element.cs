﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Element
    {
        public Element()
        {
            needMoreElements = true;
        }

        string elementName;
        public string ElementName
        {
            get { return elementName; }
            set { elementName = value; }
        }

        List<Attribute> attributes = new List<Attribute>();
        public List<Attribute> Attributes
        {
            get { return attributes; }
            set { attributes = value; }
        }

        string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        List<Element> children = new List<Element>();
        public List<Element> Children
        {
            get { return children; }
            set { children = value; }
        }

        Element parent;
        public Element Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        /// <summary>
        /// If the Element is closed, this value is false, otherwise true.
        /// </summary>
        bool needMoreElements;
        public bool NeedMoreElements
        {
            get { return needMoreElements; }
            set { needMoreElements = value; }
        }

        public override string ToString()
        {
            string output = elementName;

            if (children.Count > 0)
                output += String.Format(", Children = {0}", children.Count);
            if (attributes.Count > 0)
                output += String.Format(", Attributes = {0}", attributes.Count);
            if (text != null)
                output += String.Format(", Text = {0}", text);

            return output;
        }
    }
}
