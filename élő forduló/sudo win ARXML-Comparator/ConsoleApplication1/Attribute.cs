﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Attribute
    {
        string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        string value;
        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public override string ToString()
        {
            return String.Format("{0} = {1}", name, value);
        }
    }
}
