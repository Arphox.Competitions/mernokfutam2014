﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Data;
using System.IO;

namespace ConsoleApplication1
{
    class XmlTree
    {
        public XmlTree(string filename)
        {
            this.filename = filename;
            reader = new XmlTextReader(filename);
        }
        //test.arxml
        //test0.arxml
        //arc-stable example.arxml
        //AUTOSAR_EcucParamDef.arxml
        //EPAXMLDownload.xml
        string filename;
        List<Element> dataTree = new List<Element>();
        public List<Element> DataTree
        {
            get { return dataTree; }
            set { dataTree = value; }
        }
        Element lastElement;
        XmlTextReader reader;

        void NewElementLoading() //XmlDeclaration
        {
            Element s = new Element();
            s.ElementName = reader.Name;
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    Attribute a = new Attribute();
                    a.Name = reader.Name;
                    a.Value = reader.Value;
                    s.Attributes.Add(a);
                }
            }
            s.Parent = null;
            lastElement = s;
            dataTree.Add(s);
        }
        void SiblingLoading()
        {
            Element s = new Element();
            s.ElementName = reader.Name;
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    Attribute a = new Attribute();
                    a.Name = reader.Name;
                    a.Value = reader.Value;
                    s.Attributes.Add(a);
                }
            }
            s.Parent = lastElement.Parent;
            lastElement = s;
            dataTree.Add(s);
        }
        void Child_Loading()
        {
            Element s = new Element();
            s.ElementName = reader.Name;
            if (reader.HasAttributes)
            {
                while (reader.MoveToNextAttribute())
                {
                    Attribute a = new Attribute();
                    a.Name = reader.Name;
                    a.Value = reader.Value;
                    s.Attributes.Add(a);
                }
            }
            s.Parent = lastElement;
            lastElement.Children.Add(s);
            lastElement = s;
        }
        void Text_Loading()
        {
            lastElement.Text = reader.Value;
        }
        void EmptyElement_Loading()
        {
            Element s = new Element();
            s.ElementName = reader.Name;
            s.Parent = lastElement;
            lastElement.Children.Add(s);
        }
        void EndElement_Loading()
        {
            //záráskor mindig az utolsónak hozzáadott elementet zárjuk le, mert
            //minden Elementnek kell egy záró element! (legalábbis szerintem)
            lastElement.NeedMoreElements = false;
            while (lastElement != null && lastElement.NeedMoreElements == false)
                lastElement = lastElement.Parent;
        }
        public void Read()
        {
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.XmlDeclaration:
                        NewElementLoading();
                        lastElement.NeedMoreElements = false;//az xml-nek nincs záró tag-je
                        break;
                    case XmlNodeType.Element:
                        if (reader.IsEmptyElement) // <VALAMI/> típusú sorok lekezelése: Minden, ami ilyen formájú: <NÉV/>, azt máshogyan kell kezelni
                            EmptyElement_Loading();
                        else if (!lastElement.NeedMoreElements) //mivel már van lastElement (az XML), ezért nem lesz NullReference
                            SiblingLoading();
                        else
                            Child_Loading();
                        break;
                    case XmlNodeType.Text: //ha Text-et találunk, akkor az az utolsó Elementhez tartozik.
                        Text_Loading();
                        break;
                    case XmlNodeType.EndElement: //Az End bezárja az utolsólag megnyitott Elementet
                        EndElement_Loading();
                        break;
                    case XmlNodeType.Comment: break; //átugorjuk a kommenteket
                }
            }
        }
    }
}