﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace T_System
{
    class AdatBeolvaso
    {

        DateTime kezdetiDatum;
        DateTime vegsoDatum;

        List<BicikliAdat> bicikliAdatok;
        List<GyujtoAllomasAdat> gyujtoAllomasAdatok;


        public AdatBeolvaso(DateTime kezdetiDatum, DateTime vegsoDatum)
        {
            this.kezdetiDatum = kezdetiDatum;
            this.vegsoDatum = vegsoDatum;

            bicikliAdatok = new List<BicikliAdat>();
            gyujtoAllomasAdatok = new List<GyujtoAllomasAdat>();
        }



        public List<string> FajlokBeolvasasa(string eleresiUt)
        {
            List<string> fajlok = new List<string>();

            if (Directory.Exists(Program.eleresiUt + "/" + eleresiUt))
            {
                int kezdetiEv = kezdetiDatum.Year;
                int vegsoEv = vegsoDatum.Year;

                for (int ev = kezdetiEv; ev <= vegsoEv; ev++)
                {
                    int kezdetiHonap = 1;
                    if (ev == kezdetiEv)
                        kezdetiHonap = kezdetiDatum.Month;
                    int vegsoHonap = 12;
                    if (ev == vegsoEv)
                        vegsoHonap = vegsoDatum.Month;

                    if (Directory.Exists(Program.eleresiUt + "/" + eleresiUt + "/" + ev))
                        for (int honap = kezdetiHonap; honap <= vegsoHonap; honap++)
                        {
                            int kezdetiNap = 1;
                            if (honap == kezdetiHonap && ev == kezdetiEv)
                                kezdetiNap = kezdetiDatum.Day;
                            int vegsoNap = 31;
                            if (honap == vegsoHonap && ev == vegsoEv)
                                vegsoNap = vegsoDatum.Day;

                            if (Directory.Exists(Program.eleresiUt + "/" + eleresiUt + "/" + ev + "/" + honap))
                                for (int nap = kezdetiNap; nap <= vegsoNap; nap++)
                                    if (File.Exists(Program.eleresiUt + "/" + eleresiUt + "/" + ev + "/" + honap + "/" + nap + ".csv"))
                                    {
                                        fajlok.Add(Program.eleresiUt + "/" + eleresiUt + "/" + ev + "/" + honap + "/" + nap + ".csv");
                                    }

                        }

                }

            }

            return fajlok;
        }



    }
}
