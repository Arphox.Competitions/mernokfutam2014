﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T_System
{
    class BicikliAdat
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        DateTime letrehozva;

        public DateTime Letrehozva
        {
            get { return letrehozva; }
            set { letrehozva = value; }
        }

        int nextBicikliId;

        public int NextBicikliId
        {
            get { return nextBicikliId; }
            set { nextBicikliId = value; }
        }

        int bicikli;

        public int Bicikli
        {
            get { return bicikli; }
            set { bicikli = value; }
        }

        int nextbikeGyujtoallomasId;

        public int NextbikeGyujtoallomasId
        {
            get { return nextbikeGyujtoallomasId; }
            set { nextbikeGyujtoallomasId = value; }
        }

        int gyujtoallomas;

        public int Gyujtoallomas
        {
            get { return gyujtoallomas; }
            set { gyujtoallomas = value; }
        }



        public BicikliAdat()
        {
        }



        public BicikliAdat(int id, DateTime letrehozva, int nextBicikliId, int bicikli, int nextbikeGyujtoallomasId, int gyujtoallomas)
        {
            this.id = id;
            this.letrehozva = letrehozva;
            this.nextBicikliId = nextBicikliId;
            this.bicikli = bicikli;
            this.nextbikeGyujtoallomasId = nextbikeGyujtoallomasId;
            this.gyujtoallomas = gyujtoallomas;
        }



    }
}
