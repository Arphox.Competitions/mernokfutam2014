﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T_System
{
    class Program
    {



        public static string eleresiUt = "adatok";

        public static DateTime kezdetiDatum = new DateTime(2014, 9, 16); //vegetol
        public static DateTime vegsoDatum = new DateTime(2014, 9, 19); //eddig, ezt a napot is beleertve

        static void Main(string[] args)
        {

            Stopwatch sw = Stopwatch.StartNew();

            AdatBeolvaso adatBeolvaso = new AdatBeolvaso(kezdetiDatum, vegsoDatum);

            Legtelitettebb(adatBeolvaso);
            Feldolgozas(adatBeolvaso);

            sw.Stop();
            Console.WriteLine("Time taken: {0}ms", sw.Elapsed.Minutes + ":" + sw.Elapsed.Seconds + ":" + sw.Elapsed.Milliseconds);

            Console.ReadLine();

        }



        static List<uint> legtelitettebbek;
        static List<double> legtelitettebbekArany;
        static List<uint> legkevesbetelitettebbek;
        static List<double> legkevesbetelitettebbekArany;

        public static void Legtelitettebb(AdatBeolvaso adatBeolvaso)
        {
            legtelitettebbek = new List<uint>();
            legtelitettebbekArany = new List<double>();
            legkevesbetelitettebbek = new List<uint>();
            legkevesbetelitettebbekArany = new List<double>();

            for (int i = 0; i < 5; i++)
            {
                legtelitettebbek.Add(0);
                legtelitettebbekArany.Add(0);
                legkevesbetelitettebbek.Add(0);
                legkevesbetelitettebbekArany.Add(0);
            }

            List<uint> allomasok = new List<uint>();
            List<uint> allomasokDb = new List<uint>();
            List<uint> allomasokOsszeg = new List<uint>();

            List<string> fajlok = adatBeolvaso.FajlokBeolvasasa("biciklieloszlas");
            for (int i = 0; i < fajlok.Count; i++)
            {
                StreamReader sr = new StreamReader(fajlok[i]);
                sr.ReadLine();
                Console.WriteLine(fajlok[i] + " fajl feldolgozasa"); 
                while (!sr.EndOfStream)
                {
                    string[] sor = sr.ReadLine().Split(';');

                    if (sor[2] != String.Empty)
                    {
                        uint allomas = uint.Parse(sor[2]);
                        uint darab = uint.Parse(sor[4]);

                        int index = allomasok.IndexOf(allomas);
                        if (index >= 0)
                        {
                            allomasokDb[index]++;
                            allomasokOsszeg[index] += darab;
                        }
                        else
                        {
                            allomasok.Add(allomas);
                            allomasokDb.Add(1);
                            allomasokOsszeg.Add(darab);
                        }

                    }
                }

            }


            double[] aranyok = new double[allomasok.Count];
            for (int i = 0; i < allomasok.Count; i++)
                aranyok[i] = allomasokOsszeg[i] / allomasokDb[i];

            for (int i = 0; i < allomasok.Count; i++)
                for (int j = i + 1; j < allomasok.Count; j++)
                {
                    if (aranyok[i] < aranyok[j])
                    {   
                        allomasok[i] += allomasok[j];
                        allomasok[j] = allomasok[i] - allomasok[j];
                        allomasok[i] = allomasok[i] - allomasok[j];

                        aranyok[i] += aranyok[j];
                        aranyok[j] = aranyok[i] - aranyok[j];
                        aranyok[i] = aranyok[i] - aranyok[j];
                    }
                }


            for (int i = 0; i < 5; i++)
            {
                legtelitettebbek[i] = allomasok[i];
                legtelitettebbekArany[i] = aranyok[i];
            }

            for (int i = 0; i < 5; i++)
            {
                legkevesbetelitettebbek[i] = allomasok[allomasok.Count - i - 1];
                legkevesbetelitettebbekArany[i] = aranyok[allomasok.Count - i - 1];
            }

            //for (int i = 0; i < 5; i++)
            //    Console.WriteLine(legtelitettebbek[i] + " " + legtelitettebbekArany[i]);

            //for (int i = 0; i < 5; i++)
            //    Console.WriteLine(legkevesbetelitettebbek[i] + " " + legkevesbetelitettebbekArany[i]);
        }




        public static EgyOra[] orak;
        public static byte mostaniOra;
        public static void Feldolgozas(AdatBeolvaso adatBeolvaso)
        {
            List<string> fajlok = adatBeolvaso.FajlokBeolvasasa("biciklihely");
            mostaniOra = 24;

            bool kiirhato = false;

            orak = new EgyOra[24];
            for (int i = 0; i < 24; i++)
                orak[i] = new EgyOra();

            StreamWriter sw = new StreamWriter("outputUtvonalak.xml");
            sw.WriteLine("<?xml version=\"1.0\"?>");

            StreamWriter sw2 = new StreamWriter("outputTrend.xml");
            sw2.WriteLine("<?xml version=\"1.0\"?>");


            for (int i = 0; i < fajlok.Count; i++)
            {
                StreamReader sr = new StreamReader(fajlok[i]);
                sr.ReadLine();
                Console.WriteLine(fajlok[i] + " fajl feldolgozasa"); /////
                while (!sr.EndOfStream)
                {
                    string[] sor = sr.ReadLine().Split(';');
                    byte ora = byte.Parse(sor[1].Substring(sor[1].IndexOf(' ') + 1, 2));
                    uint bicikliId = uint.Parse(sor[3]);
                    uint allomasId = uint.Parse(sor[5]);

                    if (ora != mostaniOra)
                    {
                        if (kiirhato)
                        {
                            sw.WriteLine(StatisztikakAzOrarol(orak[ora], sor, ora));
                            sw2.WriteLine(TrendStatisztikaAzOrarol(orak[ora], sor, ora));
                        }
                        else if (ora == 22)
                            kiirhato = true;

                        orak[ora].bicikliUtak.Clear();

                        mostaniOra = ora;
                    }
                    else
                    {

                        BicikliUt bicikliUt = BenneVanEMarEgyMasikOraban(bicikliId);
                        BicikliUt bicikliUt2 = BenneVanEMarEbbeAzUtba(bicikliId);

                        if (bicikliUt == null) //ha a bicikli nincs benne egyik listaban sem akkor letrehozzuk
                        {
                            if (bicikliUt2 == null)
                            {
                                bicikliUt = new BicikliUt();
                                bicikliUt.id = bicikliId;
                                bicikliUt.celOra = ora;
                                bicikliUt.cel = allomasId;

                                orak[ora].bicikliUtak.Add(bicikliUt);
                            }
                            else
                            {
                                bicikliUt2.celOra = ora;
                                if (bicikliUt2.cel != allomasId)
                                {
                                    bicikliUt2.start = bicikliUt2.cel;
                                    bicikliUt2.startOra = bicikliUt2.celOra;
                                    bicikliUt2.cel = allomasId;
                                }
                            }
                        }
                        else
                        {
                            if (bicikliUt2 == null)
                            {
                                bicikliUt.celOra = ora;
                                orak[ora].bicikliUtak.Add(bicikliUt);
                                if (bicikliUt.cel != allomasId)
                                {
                                    bicikliUt.start = bicikliUt.cel;
                                    bicikliUt.startOra = bicikliUt.celOra;
                                    bicikliUt.cel = allomasId;
                                }
                            }
                            else
                            {
                                bicikliUt2.celOra = ora;
                                if (bicikliUt2.cel != allomasId)
                                {
                                    bicikliUt2.start = bicikliUt2.cel;
                                    bicikliUt2.startOra = bicikliUt2.celOra;
                                    bicikliUt2.cel = allomasId;
                                }
                            }
                        }

                    }

                }

                sr.ReadLine();
                sr.Close();
            }

            //int debug = 0;
            //for (int i = 0; i < orak[0].bicikliUtak.Count; i++)
            //    if (orak[0].bicikliUtak[i].start != orak[0].bicikliUtak[i].cel)
            //    {
            //        Console.WriteLine(orak[0].bicikliUtak[i].start + " " + orak[0].bicikliUtak[i].cel);
            //    }
            //    else
            //    {
            //        Console.WriteLine("xxxxxxxxxxxxxxxxxxxxxxxxxx");
            //        debug++;
            //    }
            //Console.WriteLine(debug);

            Console.WriteLine(orak[0].bicikliUtak.Count);

            sw.Close();

        }



        public static BicikliUt BenneVanEMarEgyMasikOraban(uint bicikliId) //ha benne van akkor az ertekeket is frissiti
        {
            for (int i = 0; i < 24; i++)
            {
                if (i != mostaniOra)
                {
                    for (int j = 0; j < orak[i].bicikliUtak.Count; j++)
                    {
                        if (orak[i].bicikliUtak[j].id == bicikliId)
                        {
                            return orak[i].bicikliUtak[j];
                        }
                    }
                }
            }
            return null;
        }



        public static BicikliUt BenneVanEMarEbbeAzUtba(uint bicikliId)
        {
            for (int j = 0; j < orak[mostaniOra].bicikliUtak.Count; j++)
            {
                if (orak[mostaniOra].bicikliUtak[j].id == bicikliId)
                    return orak[mostaniOra].bicikliUtak[j];
            }
            return null;
        }



        public static string StatisztikakAzOrarol(EgyOra adottOra, string[] sor, byte ora)
        {

            string output = "\r<orasAdat datum=\"" + sor[1].Substring(0, 10) + "\" ora=\"" + ora + "\">";

            List<uint> allomasok = new List<uint>();
            for (int i = 0; i < adottOra.bicikliUtak.Count; i++)
            {

                if (adottOra.bicikliUtak[i].start > 0 && !allomasok.Contains(adottOra.bicikliUtak[i].start))
                    allomasok.Add(adottOra.bicikliUtak[i].start);

                if (!allomasok.Contains(adottOra.bicikliUtak[i].cel))
                    allomasok.Add(adottOra.bicikliUtak[i].cel);

            }

            if (allomasok.Count > 0)
            {
                byte[,] honnanHova = new byte[allomasok.Count, allomasok.Count];

                for (int i = 0; i < adottOra.bicikliUtak.Count; i++)
                    if (adottOra.bicikliUtak[i].start > 0)
                        honnanHova[allomasok.IndexOf(adottOra.bicikliUtak[i].start), allomasok.IndexOf(adottOra.bicikliUtak[i].cel)]++;

                output += "\r<jellemzoUtvonalak>";

                for (int i = 0; i < allomasok.Count; i++)
                {
                    int max = 0;
                    for (int j = 1; j < allomasok.Count; j++)
                        if (honnanHova[i, j] > honnanHova[i, max])
                            max = j;

                    output += "\r<utvonal startAllomasId=\"" + allomasok[i] + "\" leggyakoribbCelAllomasId=\"" + allomasok[max] + "\">" + honnanHova[i, max] + "</utvonal>";

                }

                output += "\r</jellemzoUtvonalak>";
            }

            output += "\r</orasAdat>";
            return output;
        }
        


        public static string TrendStatisztikaAzOrarol(EgyOra adottOra, string[] sor, byte ora)
        {
            string output = "\r<orasAdat datum=\"" + sor[1].Substring(0, 10) + "\" ora=\"" + ora + "\">";

            int[] kolcsonzesekSzama = new int[5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < adottOra.bicikliUtak.Count; j++)
                {
                    if (adottOra.bicikliUtak[j].start == legtelitettebbek[i])
                        kolcsonzesekSzama[i]++;
                }
            }

            output += "\r<legtelitettebbAllomasok>";
            for (int i = 0; i < 5; i++)
            {
                output += "\r<allomas id=\"" + legtelitettebbek[i] + "\" kolcsonzesekSzama=\"" + kolcsonzesekSzama[i] + "\"></allomas>";
            }
            output += "\r</legtelitettebbAllomasok>\r";



            kolcsonzesekSzama = new int[5];
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < adottOra.bicikliUtak.Count; j++)
                {
                    if (adottOra.bicikliUtak[j].start == legkevesbetelitettebbek[i])
                        kolcsonzesekSzama[i]++;
                }
            }

            output += "\r<legkevesbetelitettebbAllomasok>";
            for (int i = 0; i < 5; i++)
            {
                output += "\r<allomas id=\"" + legtelitettebbek[i] + "\" kolcsonzesekSzama=\"" + kolcsonzesekSzama[i] + "\"></allomas>";
            }
            output += "\r</legkevesbetelitettebbAllomasok>\r";

            output += "\r</orasAdat>";
            return output;
        }


    }
}
